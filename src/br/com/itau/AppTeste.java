package br.com.itau;

import javax.swing.*;

public class AppTeste {
    public static void main(String[] args) {
        JDialog.setDefaultLookAndFeelDecorated(true);
        Elevador elevador = new Elevador();
        Object[] options;
        Object selecao;
        int opcaoSelecionada;
        int quantidadePessoa = 1;
        do {
            opcaoSelecionada = Integer.parseInt(
                    JOptionPane.showInputDialog(null, "Elevador A:\n Total de Andares: " +
                            elevador.getQuantidadeTotalAndares() + "\nAndar atual: " +
                            elevador.getAndarAtual() + "\nCapacidade máxima: " +
                            elevador.getCapacidadeMaxPessoas() + " pessoa(s)\nCapacidade atual: \"" +
                            elevador.getCapacidadeAtualPessoas() + " pessoa(s)" +
                            "\n1- Entrar" +
                            "\n2- Sair" +
                            "\n3- Subir/Descer" +
                            "\nEscolha uma opção."));

            quantidadePessoa++;
            switch (opcaoSelecionada) {
                case 1:
                    elevador.entrar(quantidadePessoa);
                    break;
                case 2:
                    if (elevador.getCapacidadeAtualPessoas() > 0) {
                        elevador.sair(1);
                    } else {
                        JOptionPane.showMessageDialog(null, "O elevador está vazio!");
                    }
                    break;
                case 3:
                    if (elevador.getCapacidadeAtualPessoas() > 0) {
                        options = new Object[]{"Subir", "Descer"};
                        selecao = JOptionPane.showInputDialog(null, "Ação:",
                                "Ação", JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

                        if (selecao.equals("Subir")) {
                            elevador.subir(Integer.parseInt(JOptionPane.showInputDialog(null, "Selecione o andar:", "Input", JOptionPane.QUESTION_MESSAGE,
                                    null, elevador.listarAndares(), 0).toString()));
                        } else {
                            elevador.descer(Integer.parseInt(JOptionPane.showInputDialog(null, "Selecione o andar:", "Input", JOptionPane.QUESTION_MESSAGE,
                                    null, elevador.listarAndares(), 0).toString()));
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "O elevador está vazio!");
                    }

                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção inválida! Escolha uma opção válida no controle");
                    break;
            }
        } while (opcaoSelecionada >= 1 && opcaoSelecionada <= 5);
    }
}
