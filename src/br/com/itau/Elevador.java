package br.com.itau;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Elevador {
    private int andarAtual = 0;
    private int maximaPosicaoElevador;
    private int minimaPosicaoElevador;
    private int capacidadeMaxPessoas;
    private int capacidadeAtualPessoas = 0;
    private int quantidadeTotalAndares = 25;
    private static int terreo = 0;

    public Elevador(){
        this.capacidadeMaxPessoas = 20;
    }

    public int getAndarAtual() {
        return andarAtual;
    }

    public void setAndarAtual(int andarAtual) {
        this.andarAtual = andarAtual;
    }

    public int getCapacidadeMaxPessoas() {
        return capacidadeMaxPessoas;
    }

    public void setCapacidadeMaxPessoas(int capacidadeMaxPessoas) {
        this.capacidadeMaxPessoas = capacidadeMaxPessoas;
    }

    public int getMaximaPosicaoElevador() {
        return maximaPosicaoElevador;
    }

    public void setMaximaPosicaoElevador(int maximaPosicaoElevador) {
        this.maximaPosicaoElevador = maximaPosicaoElevador;
    }

    public int getMinimaPosicaoElevador() {
        return minimaPosicaoElevador;
    }

    public void setMinimaPosicaoElevador(int minimaPosicaoElevador) {
        this.minimaPosicaoElevador = minimaPosicaoElevador;
    }

    public int getCapacidadeAtualPessoas() {
        return capacidadeAtualPessoas;
    }

    public void setCapacidadeAtualPessoas(int capacidadeAtualPessoas) {
        this.capacidadeAtualPessoas += capacidadeAtualPessoas;
    }

    public static int getTerreo() {
        return terreo;
    }

    public int getQuantidadeTotalAndares() {
        return quantidadeTotalAndares;
    }

    public void entrar(int quantidadePessoa){
        if(quantidadePessoa + getCapacidadeAtualPessoas() > this.getCapacidadeMaxPessoas()){
            JOptionPane.showMessageDialog(null, "Capacidade máxima de pessoas atingida!" + "\n Restam apenas "
                    + (getCapacidadeAtualPessoas() - getCapacidadeMaxPessoas()), "AVISO", JOptionPane.WARNING_MESSAGE);
        }else{
            this.setCapacidadeAtualPessoas(quantidadePessoa);
        }
    }

    public void sair(int quantidadePessoa){
        if (this.getCapacidadeAtualPessoas() > 0){
            this.setCapacidadeAtualPessoas(-quantidadePessoa);
        }else{
            JOptionPane.showMessageDialog(null, "Não foi possível efetuar essa ação!", "AVISO", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void subir(int andarSelecionado){
        if (getAndarAtual() == andarSelecionado) {
            JOptionPane.showMessageDialog(null, "Você já se encontra no andar selecionado!", "AVISO", JOptionPane.WARNING_MESSAGE);
        }else if(getAndarAtual() > andarSelecionado) {
            JOptionPane.showMessageDialog(null, "Selecione a ação DESCER o elevador!", "AVISO", JOptionPane.WARNING_MESSAGE);
        }else if(andarSelecionado <= this.getQuantidadeTotalAndares()){
            this.setAndarAtual(andarSelecionado);
        }else if (getTerreo() == andarSelecionado){
            JOptionPane.showMessageDialog(null, "Você já se encontra no terreo!", "AVISO", JOptionPane.WARNING_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null, "Você já se encontra no ultimo andar!", "AVISO", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void descer(int andarSelecionado) {
        if (getAndarAtual() == andarSelecionado) {
            JOptionPane.showMessageDialog(null, "Voce já se encontra no andar selecionado!", "AVISO", JOptionPane.WARNING_MESSAGE);
        } else if (getAndarAtual() < andarSelecionado) {
            JOptionPane.showMessageDialog(null, "Selecione a ação SUBIR o elevador!", "AVISO", JOptionPane.WARNING_MESSAGE);
        } else if (andarSelecionado <= this.getTerreo()) {
            JOptionPane.showMessageDialog(null, "Você já está no terreo!", "AVISO", JOptionPane.WARNING_MESSAGE);
        } else {
            this.setAndarAtual(andarSelecionado);
        }
    }

    public String[] listarAndares(){
        String listaAndares[] = new String[this.getQuantidadeTotalAndares()+1];

            for(int contador=0; contador < listaAndares.length;contador++) {
                listaAndares[contador] = Integer.toString(contador);
            }
        return listaAndares;
    }
}
